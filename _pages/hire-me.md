---
layout: page
title: Hire Me
permalink: /hire-me/
image: '/images/rawpixel-com-191102.jpg'
---
I offer paid services that align with my expertise.

<h2>Owner or manager? Hire me to:</h2>

* Audit your Django, Flask, or FastAPI codebase for performance bottlenecks
* Automate, review, and tune your PostgreSQL or Redis infrastructure
* Augment your data engineering team on a new web or data Python project

<h2>Software developer? Hire me to:</h2>

* Coach you on negotiating salaries and raises, interviewing, and getting promoted
* Review your web site and GitHub projects to troubleshoot hiring issues

Fill out the following form to get in touch!

<div class="form-box">
  <div class="contact-head">
  </div>
  <form class="form" action="https://formspree.io/f/xvodkbwl" method="POST">
    <div class="form__group">
      <label class="form__label screen-reader-text" for="form-name">Your Name</label>
      <input class="form__input" id="form-name" type="text" name="name" placeholder="Name" required>
    </div>
    <div class="form__group">
      <label class="form__label screen-reader-text" for="form-email">Your Email</label>
      <input class="form__input" id="form-email" type="email" name="_replyto" placeholder="Email" required>
    </div>
    <div class="form__group">
      <label class="form__label screen-reader-text" for="form-text">Your Message</label>
      <textarea class="form__input" id="form-text" name="text" rows="10" placeholder="Message" required></textarea>
    </div>
    <div class="form__group">
      <button class="button" type="submit">Send Message</button>
    </div>
  </form>
</div>
